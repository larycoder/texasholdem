from gamepack.game import Game
from gamepack.SETTING import run, raund, how_many_players
from gamepack.player import Player

# Game init
players = []
for player in range(how_many_players):
    name_player = str(input(f"Player {player + 1} name: "))
    players.append(Player(name_player))
game = Game(players)

while run:
    game.game(raund)
    raund += 1
    game.players_in_game = []
    print("-------------\nNEW GAME\n-------------")
