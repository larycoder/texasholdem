from gamepack.deck import Deck


class Table:
    def __init__(self, table_deal, deck):
        self.table = table_deal
        self.deck = deck

    def first_table_deal(self) -> list:
        self.table = [self.deck.pick_a_card() for card in range(3)]
        return self.table

    def next_table_deal(self) -> list:
        self.table.append(self.deck.pick_a_card())
        return self.table

    def show_table(self, pool):
        print("POOL:", pool)
        for card in self.table:
            print(card.show_card())





