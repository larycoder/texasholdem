from gamepack.raisee import Raise


class NextRound:
    def __init__(self, players, pool):
        self.players = players
        self.pool = pool
        self.raise_value = 0

    def whether_players_in(self, index) -> tuple:
        ins = 0
        i = index
        while 1:
            if i >= len(self.players):
                i = 0
            choice = int(
                input(f"{self.players[i].name} call <1>, raise <2> or fold <3>?")
            )
            if choice == 1:
                ins += 1
                i += 1
            elif choice == 2:
                self.raise_value = Raise().raise_value()
                i += 1
                ins = 1
                continue
            elif choice == 3:
                self.players.pop(i)
            if ins == len(self.players):
                Raise().substract_players_cash(self.players, self.raise_value)
                self.pool += Raise().pool_raiser(self.raise_value, self.players)
                return i, self.pool
