class BigOrSmall:
    def __init__(self, players, raund):
        self.players = players
        self.r = raund

    def player_with_big_and_small(self) -> None:
        if self.r == len(self.players) - 1:
            self.players[self.r].is_big_or_small += 'big'
            self.players[0].is_big_or_small += 'small'
        else:
            self.players[self.r].is_big_or_small += 'big'
            self.players[self.r + 1].is_big_or_small += 'small'
