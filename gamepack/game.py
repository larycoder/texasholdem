from itertools import count
from typing import List

from gamepack.SETTING import SMALL, BIG
from gamepack.deck import Deck
from gamepack.player import Player
from gamepack.score import Score
from gamepack.table import Table
from gamepack.deal import Deal
from gamepack.bigorsmall import BigOrSmall
from gamepack.firstround import FirstRound
from gamepack.nextround import NextRound


class Game:
    def __init__(self, players):
        self.players = players
        self.players_in_game = []
        self._pool = 0
        self._table_deal = []
        self._deck = Deck()
        self._deck.shuffle()
        self.continue_game = 1

    def game(self, raund):
        self.players_in_game += self.players
        BigOrSmall(self.players_in_game, raund).player_with_big_and_small()
        self.begin_of_raund()
        self.deal_cards()
        index_next_raund, self._pool = FirstRound(self.players_in_game, self._pool).whether_players_in(
            raund
        )
        self._table_deal = Table(self._table_deal, self._deck).first_table_deal()
        Table(self._table_deal, self._deck).show_table(self._pool)

        while self.continue_game:
            self.next_raund()
            index_next_raund, self._pool = NextRound(self.players_in_game, self._pool).whether_players_in(
                index_next_raund
            )
            self._table_deal = Table(self._table_deal, self._deck).next_table_deal()
            Table(self._table_deal, self._deck).show_table(self._pool)
            if len(self._table_deal) == 5:
                break

        for player in self.players_in_game:
            print(player.name, Score(player, self._table_deal).score())

    def begin_of_raund(self) -> None:
        self._pool = BIG + SMALL
        for player in self.players_in_game:
            if player.is_big_or_small == 'big':
                player.cash -= BIG
            if player.is_big_or_small == 'small':
                player.cash -= SMALL

    def deal_cards(self) -> None:
        Deal(self.players, self._deck).deal()

    def next_raund(self):
        input("----------------------------------------\nPRESS ENTER FOR NEXT ROUND")
