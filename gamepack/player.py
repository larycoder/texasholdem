class Player:
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.cash = 1000
        self.is_big_or_small = ''

    def show_hand(self, player):
        print("Hand of: ", player.name)
        for card in player.hand:
            print(card.show_card())

