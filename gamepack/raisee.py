from gamepack.SETTING import BIG, SMALL


class Raise:
    @staticmethod
    def raise_value():
        value = int(input("Rise value: "))
        return value

    def substract_players_cash(self, players, raise_value):
        for player in players:
            if player.is_big_or_small == 'big':
                player.cash -= (raise_value - BIG)
            if player.is_big_or_small == 'small':
                player.cash -= (raise_value - SMALL)
            if player.is_big_or_small == '':
                player.cash -= raise_value

    def pool_raiser(self, raise_value, players):
        raised_pool = 0
        for player in players:
            raised_pool += raise_value
        return raised_pool
