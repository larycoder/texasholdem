
class Deal:
    def __init__(self, players, deck):
        self.players = players
        self.deck = deck

    def deal(self) -> None:
        for player in self.players:
            for index in range(2):
                player.hand.append(self.deck.pick_a_card())