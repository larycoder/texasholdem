from dataclasses import dataclass
from typing import List, Dict, Any
from collections import Counter, OrderedDict

from gamepack.card import Card
from gamepack.player import Player


@dataclass
class Score:

    _player: Player
    _cards_on_table: List[Card]

    def score(self):
        return {
            "pair": self._check_pair(),
            "two_pairs": self._check_two_pairs(),
            "three": self._check_three(),
            "carriage": self._check_carriage(),
            "straight": self._check_straight(),
            "full": self._check_full(),
            "color": self._check_color(),
            "poker": self._check_poker(),
        }

    @property
    def _cards(self) -> List[Card]:
        cards = [card for card in self._player.hand]
        cards.extend(self._cards_on_table)
        return cards

    @property
    def _make_dict_of_all_cards(self) -> Dict[int, List[Card]]:
        dict_cards = dict()
        for card in self._cards:
            if dict_cards.get(card.rank) is not None:
                dict_cards[card.rank].append(card)
            else:
                dict_cards[card.rank] = [card]
        return OrderedDict(sorted(dict_cards.items(), reverse=True))

    def _get_dict_of_cards_with_score(
        self, num_of_cards: int
    ) -> Dict[int, List[Card]] or None:
        for rank, card_list in self._make_dict_of_all_cards.items():
            if len(card_list) == num_of_cards:
                return {rank: card_list}

    def _check_pair(self) -> Dict[int, List[Card]] or None:
        return self._get_dict_of_cards_with_score(2)

    def _check_two_pairs(self) -> Dict[int, List[Card]] or None:
        two_pairs_dict = dict()
        for rank, card_list in self._make_dict_of_all_cards.items():
            if len(card_list) == 2:
                two_pairs_dict[rank] = card_list
                if len(two_pairs_dict) == 2:
                    return two_pairs_dict

    def _check_three(self) -> Dict[int, List[Card]] or None:
        return self._get_dict_of_cards_with_score(3)

    def _check_carriage(self) -> Dict[int, List[Card]] or None:
        return self._get_dict_of_cards_with_score(4)

    def _check_straight(self) -> List[Card] or None:
        cards = sorted(self._cards, key=lambda k: k.rank, reverse=True)
        tmp_card_rank = cards[0].rank
        tmp_list_of_cards = []
        tmp_list_of_index = []
        for index in range(1, len(cards)):
            if tmp_card_rank - 1 == cards[index].rank:
                tmp_list_of_cards.append(cards[index])
                tmp_list_of_index.append(index)
            else:
                tmp_list_of_cards = []
                tmp_list_of_index = []
            tmp_card_rank = cards[index].rank
        if len(tmp_list_of_cards) >= 4:
            tmp_list_of_cards.insert(0, cards[tmp_list_of_index[0] - 1])
            return tmp_list_of_cards

    def _check_full(self) -> Dict[int, List[Card]] or None:
        pair = self._check_pair()
        three = self._check_three()
        full_dict = {}
        if pair and three:
            full_dict.update(pair)
            full_dict.update(three)
            return full_dict

    def _check_color(self) -> List[Card] or None:
        dict_cards = dict()
        for card in self._cards:
            if dict_cards.get(card.suite) is not None:
                dict_cards[card.suite].append(card)
            else:
                dict_cards[card.suite] = [card]
        for suite, card_list in dict_cards.items():
            card_list = sorted(card_list, key=lambda k: k.rank,reverse=True)
            if len(card_list) >= 5:
                return card_list[:5]

    def _check_poker(self) -> List[Card] or None:
        straight = self._check_straight()
        if self._check_color() and straight:
            return straight

