from gamepack.SETTING import BIG, SMALL
from gamepack.raisee import Raise


class FirstRound:
    def __init__(self, players, pool):
        self.pool = pool
        self.players = players
        self.raise_value = BIG

    def whether_players_in(self, raund) -> tuple:
        ins = 0
        i = raund + 1
        while 1:
            if i >= len(self.players):
                i = 0
            choice = int(
                input(f"{self.players[i].name} call <1>, raise <2> or fold <3>?")
            )
            if choice == 1:
                ins += 1
                i += 1
            elif choice == 2:
                self.raise_value = Raise().raise_value()
                i += 1
                ins = 0
                continue
            elif choice == 3:
                self.players.pop(i)
            if ins == len(self.players) - 1:
                Raise().substract_players_cash(self.players, self.raise_value)
                self.pool = Raise().pool_raiser(self.raise_value, self.players)
                return i, self.pool

