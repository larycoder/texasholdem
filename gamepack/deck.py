import random
from gamepack.card import Card
from gamepack.SETTING import RANKS, SUITS


def deck_deko(method):
    def wrapper(self):
        print("Deck generated, dealing cards ...")
        method(self)
    return wrapper


class Deck:
    def __init__(self):
        self.cards = []
        self.generate_deck()

    @deck_deko
    def generate_deck(self):
        self.cards = [Card(fig, col) for col in RANKS for fig in SUITS]

    def show_deck(self):
        for card in self.cards:
            print(card)

    def shuffle(self):
        random.shuffle(self.cards)

    def pick_a_card(self):
        return self.cards.pop()

    def __str__(self):
        self.show_deck()
