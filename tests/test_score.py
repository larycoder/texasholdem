from typing import List, Dict, Any

import pytest

from gamepack.card import Card
from gamepack.player import Player
from gamepack.score import Score


class TestScore:
    @pytest.fixture(autouse=True)
    def setUp(self) -> None:
        self.player = Player("player")

    def test_score_contain_pair(self):
        # Given
        score = Score(
            self.player, [Card(1, 2), Card(1, 4), Card(2, 8), Card(2, 6), Card(1, 9)]
        )
        self.player.hand = [Card(1, 5), Card(2, 2)]

        # When
        output = score.score()

        # Then
        assert self._expected(pair={2: [Card(2, 2), Card(1, 2)]}) == output

    def test_score_contain_three(self):
        # Given
        score = Score(
            self.player, [Card(1, 2), Card(1, 4), Card(2, 8), Card(2, 6), Card(1, 9)]
        )
        self.player.hand = [Card(3, 2), Card(2, 2)]

        # When
        output = score.score()

        # Then
        assert self._expected(three={2: [Card(3, 2), Card(2, 2), Card(1, 2)]}) == output

    @pytest.mark.parametrize(
        "cards, expected_cards",
        [
            (
                [Card(1, 2), Card(1, 4), Card(2, 8), Card(2, 6), Card(1, 9)],
                [Card(1, 12), Card(1, 11), Card(1, 9), Card(1, 4), Card(1, 2)],
            ),
            (
                [Card(1, 2), Card(1, 4), Card(1, 8), Card(1, 6), Card(1, 9)],
                [
                    Card(1, 12),
                    Card(1, 11),
                    Card(1, 9),
                    Card(1, 8),
                    Card(1, 6),
                ],
            ),
        ],
    )
    def test_score_color(self, cards, expected_cards):
        # Given
        self.player.hand = [Card(1, 11), Card(1, 12)]

        score = Score(self.player, cards)

        # When
        output = score.score()

        # Then
        assert self._expected(color=expected_cards) == output

    @pytest.mark.parametrize(
        "cards, expected_cards",
        [
            (
                [Card(1, 2), Card(1, 4), Card(1, 8), Card(2, 6), Card(1, 9)],
                [Card(1, 11), Card(1, 9), Card(1, 8), Card(1, 4), Card(1, 2)],
            ),
            (
                [Card(1, 2), Card(1, 4), Card(1, 8), Card(1, 6), Card(1, 9)],
                [
                    Card(1, 11),
                    Card(1, 9),
                    Card(1, 8),
                    Card(1, 6),
                    Card(1, 4),
                ],
            ),
        ],
    )
    def test_score_color_when_player_has_different_ranks_on_hand(
        self, cards, expected_cards
    ):
        # Given
        self.player.hand = [Card(1, 11), Card(2, 12)]

        score = Score(self.player, cards)

        # When
        output = score.score()

        # Then
        assert self._expected(color=expected_cards) == output

    def test_score_color_when_player_is_only_in_table_of_cards_from_table(self):
        # Given
        self.player.hand = [Card(3, 11), Card(2, 12)]
        score = Score(
            self.player, [Card(1, 2), Card(1, 4), Card(1, 8), Card(1, 6), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                color=[Card(1, 9), Card(1, 8), Card(1, 6), Card(1, 4), Card(1, 2)]
            )
            == output
        )

    def test_score_carriage(self):
        # Given
        self.player.hand = [Card(2, 4), Card(3, 3)]
        score = Score(
            self.player, [Card(1, 2), Card(2, 2), Card(3, 2), Card(4, 2), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                carriage={2: [Card(1, 2), Card(2, 2), Card(3, 2), Card(4, 2)]}
            )
            == output
        )

    def test_score_carriage_when_player_has_pair_on_hand(self):
        # Given
        self.player.hand = [Card(2, 2), Card(3, 2)]
        score = Score(
            self.player, [Card(1, 2), Card(2, 4), Card(3, 5), Card(4, 2), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                carriage={2: [Card(2, 2), Card(3, 2), Card(1, 2), Card(4, 2)]}
            )
            == output
        )

    def test_check_two_pairs(self):
        # Given
        self.player.hand = [Card(2, 2), Card(3, 2)]
        score = Score(
            self.player, [Card(1, 3), Card(2, 3), Card(3, 5), Card(4, 6), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                pair={3: [Card(1, 3), Card(2, 3)]},
                two_pairs={3: [Card(1, 3), Card(2, 3)], 2: [Card(2, 2), Card(3, 2)]},
            )
            == output
        )

    def test_check_two_pairs_when_is_more_than_two_pairs_in_the_cards(self):
        # Given
        self.player.hand = [Card(2, 2), Card(3, 2)]
        score = Score(
            self.player, [Card(1, 3), Card(2, 3), Card(3, 6), Card(4, 6), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                pair={6: [Card(3, 6), Card(4, 6)]},
                two_pairs={6: [Card(3, 6), Card(4, 6)], 3: [Card(1, 3), Card(2, 3)]},
            )
            == output
        )

    def test_check_full(self):
        # Given
        self.player.hand = [Card(2, 2), Card(2, 6)]
        score = Score(
            self.player, [Card(1, 3), Card(2, 3), Card(3, 6), Card(4, 6), Card(1, 9)]
        )

        # When
        output = score.score()

        # Then
        assert (
            self._expected(
                pair={3: [Card(1, 3), Card(2, 3)]},
                three={6: [Card(2, 6), Card(3, 6), Card(4, 6)]},
                full={
                    3: [Card(1, 3), Card(2, 3)],
                    6: [Card(2, 6), Card(3, 6), Card(4, 6)],
                },
            )
            == output
        )

    def test_check_straight(self):
        # Given
        score = Score(
            self.player, [Card(1, 14), Card(1, 11), Card(2, 4), Card(2, 5), Card(1, 6)]
        )
        self.player.hand = [Card(1, 2), Card(2, 3)]

        # When
        output = score.score()

        # Then
        assert self._expected(straight=[Card(1, 6), Card(2, 5), Card(2, 4), Card(2, 3), Card(1, 2)]) == output

    def test_check_straight_is_masked(self):
        # Given
        score = Score(
            self.player, [Card(1, 14), Card(1, 13), Card(2, 4), Card(2, 5), Card(1, 6)]
        )
        self.player.hand = [Card(1, 2), Card(2, 3)]

        # When
        output = score.score()

        # Then
        assert self._expected(straight=[Card(1, 6), Card(2, 5), Card(2, 4), Card(2, 3), Card(1, 2)]) == output

    def test_check_poker(self):
        # Given
        score = Score(
            self.player, [Card(1, 14), Card(1, 13), Card(2, 4), Card(2, 5), Card(2, 6)]
        )
        self.player.hand = [Card(2, 2), Card(2, 3)]
        expected = [Card(2, 6), Card(2, 5), Card(2, 4), Card(2, 3), Card(2, 2)]
        # When
        output = score.score()

        # Then
        assert self._expected(poker=expected,color=expected,straight=expected) == output

    @staticmethod
    def _expected(
        pair: Dict[int, List[Card]] or None = None,
        two_pairs: Dict[int, List[Card]] or None = None,
        three: Dict[int, List[Card]] or None = None,
        carriage: Dict[int, List[Card]] or None = None,
        straight: Dict[int, List[Card]] or None = None,
        full: Dict[Any, List[Card]] or None = None,
        color: List[Card] or None = None,
        poker: List[Card] or None = None,
    ):
        return {
            "pair": pair,
            "two_pairs": two_pairs,
            "three": three,
            "carriage": carriage,
            "straight": straight,
            "full": full,
            "color": color,
            "poker": poker,
        }

