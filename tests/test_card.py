import pytest

from gamepack.card import Card


@pytest.mark.parametrize(
    "card, expected",
    [
        (Card(1, 2), "♣ of 2"),
        (Card(3, 11), "♡ of J"),
        (Card(4, 12), "♠ of Q"),
        (Card(2, 13), "♢ of K"),
        (Card(4, 14), "♠ of A"),
    ],
)
def test_card__str__(card, expected):
    assert card.__str__() == expected


@pytest.mark.parametrize(
    "card, expected",
    [
        (Card(1, 2), "♣ of 2"),
        (Card(3, 11), "♡ of J"),
        (Card(4, 12), "♠ of Q"),
        (Card(2, 13), "♢ of K"),
        (Card(4, 14), "♠ of A"),
    ],
)
def test_show_card(card, expected):
    assert card.show_card() == expected


def test_suite():
    suite = 1

    card = Card(suite, 2)

    assert card.suite == suite


def test_rank():
    rank = 2

    card = Card(1, rank)

    assert card.rank == rank

@pytest.mark.parametrize(
    "suite, expected",
    [
        (0, AttributeError),
        (5, AttributeError)
    ],
)
def test_whether_card_raises_exception_for_suite(suite, expected):
    with pytest.raises(expected):
        Card(suite, 2)

@pytest.mark.parametrize(
    "rank, expected",
    [
        (1, AttributeError),
        (15, AttributeError)
    ],
)
def test_whether_card_raises_exception_for_rank(rank, expected):
    with pytest.raises(expected):
        Card(3, rank)