from unittest import TestCase

from gamepack.deck import Deck
from gamepack.SETTING import RANKS, SUITS


class TestDeck(TestCase):
    def setUp(self) -> None:
        self.deck = Deck()

    def test_generate_deck_whether_has_all_cards(self):
        amount_of_cards = len(RANKS) * len(SUITS)

        self.deck.generate_deck()

        self.assertEqual(len(self.deck.cards), amount_of_cards)

    def test_whether_poping_card_from_deck_works(self):
        start_len = len(self.deck.cards)
        self.deck.pick_a_card()

        self.assertEqual(start_len-1, len(self.deck.cards))
