from unittest import TestCase

from gamepack.deck import Deck
from gamepack.game import Deal
from gamepack.player import Player
from gamepack.game import Game


class TestDeal(TestCase):
    def setUp(self) -> None:
        self.deck = Deck()
        self.player1 = Player("Tom")
        self.player2 = Player("Jimmy")
        self.big_and_small = {"big": self.player1, "small": self.player2}
        self.deal = Deal([self.player1, self.player2], self.deck)

    def test_whether_players_hands_are_different(self):
        self.deal.deal()

        self.assertNotEqual(self.player1.hand, self.player2.hand)

    def test_begin_of_round(self):
        Game([self.player1, self.player2]).begin_of_raund(self.big_and_small)

        self.assertEqual(self.player1.cash, 900) # BIG
        self.assertEqual(self.player2.cash, 950) # SMALL




